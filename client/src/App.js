import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';

import Header from './Components/Layout/Header/Header';

const LazyMainPage = lazy(() => import('./Pages/Main/MainPage'));
const LazyProfile = lazy(() => import('./Components/Profile/Profile'));
const LazyShop = lazy(() => import('./Components/Shop/Shop'));
const LazyGallery = lazy(() => import('./Components/Galery/Galery'));
const LazyAdminProfile = lazy(() => import('./Components/Profile/AdminProfile'));
const LazyNewsList = lazy(() => import('./Components/News/NewsList'));
const LazyNews = lazy(() => import('./Components/News/News'));
const LazyWork = lazy(() => import('./Components/Work/WorkItem'));
const LazyWorkList = lazy(() => import('./Components/Work/WorkList'));
const LazyDIY = lazy(() => import('./Components/DIY/DIY'));
const LazyShopItem = lazy(() => import('./Components/Shop/ShopItem'));
const LazySearch = lazy(() => import('./Components/Search/Search'));
const LazyBasket = lazy(() => import('./Pages/Basket/BasketPage'));
const LazyManager = lazy(() => import('./Pages/Manager/ManagerPage'));


function App() {
  return (
    <div className="App">
      <Header />
      <div>
        <Suspense fallback={<div>Loading...</div>}>
          <Route exact path="/" component={LazyMainPage} />
          <Route exact path="/profile/user/:user_id" component={(state) => <LazyProfile {...state} />} />
          <Route exact path="/manager" component={LazyManager} />
          <Route exact path="/search" component={(state) => <LazySearch {...state} />} />
          <Route exact path="/admin" component={LazyAdminProfile} />
          <Route exact path="/basket" component={LazyBasket} />
          <Route exact path="/work/:work_id" component={LazyWork} />
          <Route exact path="/work" component={LazyWorkList} />
          <Route exact path="/shop" component={LazyShop} />
          <Route exact path="/shop/item/:item_id" component={LazyShopItem} />
          <Route exact path="/diy" component={LazyDIY} />
          <Route exact path="/news" component={LazyNewsList} />
          <Route exact path="/news/item/:news_id" component={LazyNews} />
          <Route exact path="/galery" component={LazyGallery} />
        </Suspense>
      </div>
    </div>
  );
}


export default App;
