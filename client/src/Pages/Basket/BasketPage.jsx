import React from 'react';
import { connect } from 'react-redux';
import {
  Avatar,
  Button,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import useAsyncEffect from 'use-async-effect';
import { deleteItem, getBasket } from '../../redux/actions/basketActions';
import { createOrder } from '../../redux/actions/orderActions';
import {LOCALIZATION} from "../../Localization/localization";

function BasketPage(props) {
  const { basketReducer } = props;

  useAsyncEffect(async () => {
    await props.getBasket();
  }, []);

  const deleteDish = (dish) => async () => {
    const updateItems = props.basketReducer.basket.shopItems
      .filter((datum) => datum._id !== dish._id);
    await props.deleteItem(updateItems);
  };

  const addOrder = (data) => async () => {
    await props.createOrder({
      seller: data.item.user,
      shopItem: data.item._id,
    });
    await props.getBasket();
  };

  return (
    <div className="container mt-5">
      {basketReducer.basket && basketReducer.basket.shopItems.length > 0
        ? (
          <div className="row pt-5">
            <div className="col">
              <List>
                {basketReducer.basket.shopItems.map((item) => (
                  <div key={item._id}>
                    <ListItem className="row d-flex">
                      <ListItemAvatar>
                        <Avatar src={item.item.titleImage} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={item.item.title}
                      />
                      <ListItemText
                        primary={`${LOCALIZATION.BASKET_COST_ITEM}: ${item.item.cost}$`}
                        secondary={`${LOCALIZATION.BASKET_NUMBER}: ${item.count}`}
                      />
                      <ListItemText
                        primary={`${LOCALIZATION.BASKET_TOTAL_ITEM}: ${item.item.cost * item.count}$`}
                      />
                    </ListItem>
                    <div className="d-flex justify-content-end mr-2 mb-2">
                      <Button
                        className="mr-2"
                        variant="contained"
                        onClick={() => window.open(`/shop/item/${item.item._id}`, '_blank')}
                      >
                        {LOCALIZATION.BASKET_ITEM_INFORMATION}
                      </Button>
                      <Button
                        className="mr-2"
                        variant="contained"
                        color="primary"
                        onClick={addOrder(item)}
                      >
                        {LOCALIZATION.BASKET_ITEM_BYE}
                      </Button>
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={deleteDish(item)}
                      >
                        {LOCALIZATION.BASKET_ITEM_DELETE}
                      </Button>
                    </div>
                    <Divider variant="middle" />
                  </div>
                ))}
              </List>
            </div>

            <div className="rounded p-3 border col-md-3
                d-flex flex-column justify-content-center text-center"
            >
              <h3>{LOCALIZATION.BASKET_YOUR_BASKET}</h3>
              <p>
                {LOCALIZATION.BASKET_PRODUCTS_COUNT}
                :
                {' '}
                {basketReducer.basket.shopItems.length}
              </p>
              <p>
                {LOCALIZATION.BASKET_TOTAL_PRICE}
                :
                {' '}
                {basketReducer.basket.price}
                $
              </p>
            </div>
          </div>
        )
        : (
          <div style={{ height: '85vh' }} className="container d-flex justify-content-center align-items-center">
            <Typography variant="h2" gutterBottom>
              {LOCALIZATION.BASKET_EMPTY}
            </Typography>
          </div>
        )}
    </div>
  );
}

BasketPage.defaultProps = {
  basketReducer: {},
  getBasket: () => {},
  deleteItem: () => {},
  createOrder: () => {},
};

BasketPage.propTypes = {
  basketReducer: PropTypes.object,
  getBasket: PropTypes.func,
  deleteItem: PropTypes.func,
  createOrder: PropTypes.func,
};

const mapStateToProps = (state) => ({
  basketReducer: state.basketReducer,
});

const actions = {
  getBasket,
  deleteItem,
  createOrder,
};

export default connect(mapStateToProps, actions)(BasketPage);
