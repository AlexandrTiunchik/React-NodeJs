import React, { useState } from 'react';
import useAsyncEffect from 'use-async-effect';

import ManagerApi from '../../Services/Api/managerApi';

import ShopItemsTable from './Components/ShopItemsTable';
import ManagerTab from './Components/ManagerTab';
import WorkItemTable from './Components/WorkTable';
import ShopSellerOrders from './Components/ShopOrders';
import OrderApi from '../../Services/Api/orderApi';
import CustomerOrdersTable from './Components/CustomerOrderTable';

function ManagerPage(props) {
  const [shopItems, setShopItems] = useState();
  const [workItems, setWorkItems] = useState();
  const [sellerOrdersItems, setSellerOrdersItems] = useState();
  const [customerOrdersItems, setCustomerOrdersItems] = useState();

  useAsyncEffect(async () => {
    const [
      shopItemsResponse,
      workItemsResponse,
      sellerOrdersResponse,
      customerOrdersResponse,
    ] = await Promise.all([
      new ManagerApi().getShopItems(),
      new ManagerApi().getWorkItems(),
      new OrderApi().getSellerOrders(),
      new OrderApi().getCustomerOrders(),
    ]);

    setWorkItems(workItemsResponse.data);
    setShopItems(shopItemsResponse.data);
    setSellerOrdersItems(sellerOrdersResponse.data);
    setCustomerOrdersItems(customerOrdersResponse.data);
  }, []);

  const shopTab = (
    <div>
      <ShopItemsTable shopItems={shopItems} />
    </div>
  );

  const workTab = (
    <div>
      <WorkItemTable workItems={workItems} />
    </div>
  );

  const orderTab = (
    <div>
      <ShopSellerOrders sellerOrdersItems={sellerOrdersItems} />
    </div>
  );

  const customerTab = (
    <div>
      <CustomerOrdersTable customerOrdersItems={customerOrdersItems} />
    </div>
  );

  return (
    <div className="p-3">
      <ManagerTab
        shopTab={shopTab}
        workTab={workTab}
        orderTab={orderTab}
        customerTab={customerTab}
      />
    </div>
  );
}

export default ManagerPage;
