import React from 'react';
import PropTypes from 'prop-types';

import Typography from '../../../Components/assets/Typography';
import { LOCALIZATION } from '../../../Localization/localization';
import CardProduct from '../../../Components/Elements/Shop/CardProduct';


function ShopSection(props) {
  const { shopItems } = props;

  return (
    <div>
      <Typography variant="h4" marked="center" align="center" component="h2" className="mt-5">
        {LOCALIZATION.MAIN_PAGE_SHOP_TITLE}
      </Typography>
      <div style={{ maxWidth: 1000, margin: 'auto' }} className="mt-4 mb-5">
        <Typography variant="h5" marked="center" align="center" component="h2">
          {LOCALIZATION.MAIN_PAGE_SHOP_TITLE}
        </Typography>
      </div>
      <div className="d-flex flex-wrap justify-content-center">
        {shopItems.map((item) => (
          <div
            style={{ backgroundColor: '#f0eef0', borderRadius: 10 }}
            className="ml-3 mr-3 p-1 mb-2"
            key={item._id}
          >
            <CardProduct shopItems={item} />
          </div>
        ))}
      </div>
    </div>
  );
}

ShopSection.defaultProps = {
  shopItems: [],
};

ShopSection.propTypes = {
  shopItems: PropTypes.array,
};

export default ShopSection;
