import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

import GridContainer from '../../../Components/Elements/MainElem/Elem/Grid/GridContainer';
import GridItem from '../../../Components/Elements/MainElem/Elem/Grid/GridItem';
import { LOCALIZATION } from '../../../Localization/localization';
import Parallax from '../../../Components/Elements/MainElem/Elem/Parallax/Parallax';
import { LANDING_PAGE_STYLE } from '../../../Components/assets/jss/material-kit-react/views/landingPage';


function PresentationSection(props) {
  const { classes, quote, mainImage } = props;

  return (
    <Parallax filter image={mainImage}>
      <div className={classes.container}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={6}>
            <h1 className={classes.title}>{LOCALIZATION.MAIN_PAGE__TITLE}</h1>
            <h4>
              {quote}
            </h4>
            <br />
          </GridItem>
        </GridContainer>
      </div>
    </Parallax>
  );
}

PresentationSection.defaultProps = {
  classes: {},
  quote: '',
  mainImage: '',
};

PresentationSection.propTypes = {
  classes: PropTypes.object,
  quote: PropTypes.string,
  mainImage: PropTypes.string,
};

export default withStyles(LANDING_PAGE_STYLE)(PresentationSection);
