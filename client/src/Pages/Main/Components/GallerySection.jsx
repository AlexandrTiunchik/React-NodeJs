import React from 'react';
import PropTypes from 'prop-types';

import Typography from '../../../Components/assets/Typography';
import { LOCALIZATION } from '../../../Localization/localization';
import styles from '../../../Components/Elements/MainElem/style.module.css';


function GallerySection(props) {
  const { images } = props;

  return (
    <div>
      <Typography variant="h4" marked="center" align="center" component="h2" className="mt-5 mb-1">
        {LOCALIZATION.MAIN_PAGE_GALLERY_TITLE}
      </Typography>
      <div style={{ maxWidth: 1000, margin: 'auto' }} className="mt-2 mb-4">
        <Typography variant="h5" marked="center" align="center" component="h2">
          {LOCALIZATION.MAIN_PAGE_GALLERY_TEXT}
        </Typography>
      </div>

      <div
        style={{ overflow: 'auto' }}
        className="d-flex justify-content-center"
      >
        {images && images.map((image) => (
          <div key={image._id}>
            <div
              style={{ backgroundImage: `url(${image.contentRef})` }}
              className={`img-thumbnail ${styles.img}`}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

GallerySection.defaultProps = {
  images: [],
};

GallerySection.propTypes = {
  images: PropTypes.array,
};

export default GallerySection;
