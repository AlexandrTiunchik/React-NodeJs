import { LOCALIZATION } from '../../../Localization/localization';

export const LABOR_EXCHANGE_CATEGORY = [
  {
    url: 'https://gazeta.a42.ru/uploads/922/92233500-13f5-11e9-af6d-a1cd63783566.jpeg',
    title: `${LOCALIZATION.MAIN_PAGE_FREELANCE_CATEGORY_MUSIC}`,
    width: '40%',
    filter: 'music',
  },
  {
    url: 'https://lh6.ggpht.com/HlgucZ0ylJAfZgusynnUwxNIgIp5htNhShF559x3dRXiuy_UdP3UQVLYW6c',
    title: `${LOCALIZATION.MAIN_PAGE_FREELANCE_CATEGORY_ART}`,
    width: '20%',
    filter: 'art',
  },
  {
    url: 'https://www.cinema4dtutorial.net/wp-content/uploads/2014/06/ieoenr-640x250.jpg',
    title: `${LOCALIZATION.MAIN_PAGE_FREELANCE_CATEGORY_3D_MODEL}`,
    width: '40%',
    filter: '3d model',
  },
  {
    url: 'https://nofilmschool.com/sites/default/files/styles/article_wide/public/film_analysis.jpg?itok=iwgnGurT',
    title: `${LOCALIZATION.MAIN_PAGE_FREELANCE_CATEGORY_CREATING_A_VIDEO}`,
    width: '38%',
    filter: 'video making',
  },
  {
    url: 'https://salarieshub.com/wp-content/uploads/2016/10/graphic-designer-working.jpg',
    title: `${LOCALIZATION.MAIN_PAGE_FREELANCE_CATEGORY_DESIGN}`,
    width: '38%',
    filter: 'design',
  },
  {
    url: 'http://caitlintphotography.com/wp-content/uploads/2017/01/photography.jpg',
    title: `${LOCALIZATION.MAIN_PAGE_FREELANCE_CATEGORY_PHOTOGRAPHY}`,
    width: '24%',
    filter: 'photography',
  },
];
