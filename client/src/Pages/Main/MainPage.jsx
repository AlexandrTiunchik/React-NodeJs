import React, { PureComponent } from 'react';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
import axios from 'axios';
import PropTypes from 'prop-types';

import NewsBlock from '../../Components/Elements/MainElem/NewsBlock';
import Footer from '../../Components/Elements/MainElem/Footer';
import ProgressBar from '../../Components/Elements/Publick/ProgressBar';
import Quotes from '../../Constants/quotes';
import MainImages from '../../Constants/mainImages';
import GallerySection from './Components/GallerySection';
import ShopSection from './Components/ShopSection';
import LaborExchangeSection from './Components/LaborExchangeSection';
import PresentationSection from './Components/PresentationSection';
import { LANDING_PAGE_STYLE } from '../../Components/assets/jss/material-kit-react/views/landingPage';


class MainPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      music: '',
      art: '',
      film: '',
      mainNews: '',
      shopItems: '',
      isDataLoaded: false,
      quote: '',
      mainImage: '',
      images: '',
    };
  }

  componentDidMount() {
    Promise.all([
      axios.get('/api/news/music'),
      axios.get('/api/news/art'),
      axios.get('/api/news/film'),
      axios.get('/api/news/mainnews'),
      axios.get('api/shop/three'),
      axios.get('/api/galery/image'),
    ]).then((response) => {
      const [music, art, film, mainNews, shop, images] = response;
      this.setState({
        ...this.state,
        music: music.data,
        art: art.data,
        film: film.data,
        images: images.data,
        mainNews: mainNews.data,
        shopItems: shop.data,
        quote: Quotes[Math.floor(Math.random() * (Quotes.length))],
        mainImage: MainImages[Math.floor(Math.random() * (Quotes.length))],
        isDataLoaded: true,
      });
    });
  }

  render() {
    const { classes } = this.props;
    const {
      isDataLoaded,
      quote,
      mainImage,
      images,
      shopItems,
    } = this.state;

    return (
      <>
        {
          isDataLoaded
            ? (
              <div>
                <PresentationSection
                  quote={quote}
                  mainImage={mainImage}
                />

                <div className={classNames(classes.main, classes.mainRaised)}>
                  <NewsBlock news={this.state} />
                  <GallerySection images={images} />
                  <ShopSection shopItems={shopItems} />
                  <LaborExchangeSection />
                </div>

                <Footer />
              </div>
            )
            : (
              <div style={{ height: '95vh' }} className="d-flex justify-content-center">
                <ProgressBar />
              </div>
            )
        }
      </>
    );
  }
}

MainPage.defaultProps = {
  classes: {},
};

MainPage.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(LANDING_PAGE_STYLE)(MainPage);
