import React, { Component } from 'react';
import firebase from 'firebase';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';

import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton';
import Snackbar from '../Components/Elements/Publick/Snackbar';
import {LOCALIZATION} from "../Localization/localization";


const config = {
  apiKey: 'AIzaSyC5l8ArMOi5_Gs-LCeI1A4QeOg_tLIObDM',
  authDomain: 'artgalery-fe9fd.firebaseapp.com',
  databaseURL: 'https://artgalery-fe9fd.firebaseio.com',
  storageBucket: 'artgalery-fe9fd.appspot.com',
};

firebase.initializeApp(config);

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUploading: false,
      progress: 0,
      avatarURL: '',
      success: false,
    };
  }

  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });

  handleProgress = (progress) => this.setState({ progress });

  handleUploadError = (error) => {
    this.setState({ isUploading: false });
  };

  handleUploadSuccess = (filename) => {
    const {
      newTitle,
      title,
      text,
      category,
      titleImage,
      addNews,
      upload,
      uploadImgList,
      uploadVideo,
    } = this.props;

    this.setState({ avatar: filename, progress: 100, isUploading: false });
    firebase
      .storage()
      .ref('images')
      .child(filename)
      .getDownloadURL()
      .then((url) => {
        if (newTitle) {
          axios.post('/api/user/userWork', {
            title,
            text,
            titleImage,
            contentType: category,
            contentRef: url,
          });
        }


        if (addNews) {
          upload(url);
        }

        if (uploadImgList) {
          uploadImgList(url);
        }

        if (uploadVideo) {
          uploadVideo(url);
        }

        this.setState({ success: true });
      });
  };

  render() {
    const { progress, isUploading, success } = this.state;

    return (
      <>
        {
        isUploading
          ? (
            <div className="d-flex align-items-center flex-column justify-content-center">
              <p>
                Загрузка:
                {progress}
                %
              </p>
              <CircularProgress
                variant="static"
                value={progress}
              />
            </div>
          )
          : (
            <div className="text-center mt-3">
              <CustomUploadButton
                accept=""
                storageRef={firebase.storage().ref('images')}
                onUploadStart={this.handleUploadStart}
                onUploadError={this.handleUploadError}
                onUploadSuccess={this.handleUploadSuccess}
                onProgress={this.handleProgress}
                style={{
                  backgroundColor: 'steelblue', color: 'white', padding: 10, borderRadius: 4,
                }}
              >
                {LOCALIZATION.PROFILE_EDIT_ABOUT_CHANGE_AVATAR}
              </CustomUploadButton>
            </div>
          )
      }

        {success
          ? <Snackbar textMessage="Загрузка завершена" />
          : null}
      </>
    );
  }
}

ProfilePage.defaultProps = {
  newTitle: '',
  title: '',
  text: '',
  category: '',
  titleImage: '',
  addNews: () => {},
  upload: () => {},
  uploadImgList: () => {},
  uploadVideo: () => {},
};

ProfilePage.propTypes = {
  newTitle: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string,
  category: PropTypes.string,
  titleImage: PropTypes.string,
  addNews: PropTypes.func,
  upload: PropTypes.func,
  uploadImgList: PropTypes.func,
  uploadVideo: PropTypes.func,
};

export default ProfilePage;
