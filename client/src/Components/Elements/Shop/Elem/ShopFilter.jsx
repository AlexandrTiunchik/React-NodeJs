import React, { Component } from 'react'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import {LOCALIZATION} from "../../../../Localization/localization";

export default class JobSelect extends Component {
  state = {
    category: '',
    title: '',
    sortBy: ''
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    const { filterItem } = this.props;

    return (
      <div className='p-2'>
        <FormControl fullWidth>
          <InputLabel htmlFor="sortBy-helper">{LOCALIZATION.SHOP_FILTER_SORTING_LABEL}</InputLabel>
          <Select
            value={this.state.sortBy}
            onChange={this.handleChange('sortBy')}
            input={<Input name="sortBy" id="sortBy-helper" />}
            fullWidth
          >
            <MenuItem value='date'>{LOCALIZATION.SHOP_FILTER_SORTING_TYPE_DATE}</MenuItem>
            <MenuItem value='salary'>{LOCALIZATION.SHOP_FILTER_SORTING_TYPE_COST}</MenuItem>
          </Select>
        </FormControl>

        <FormControl className='mt-4' fullWidth>
          <InputLabel htmlFor="category-helper">{LOCALIZATION.SHOP_FILTER_CATEGORY}</InputLabel>
          <Select
            value={this.state.category}
            onChange={this.handleChange('category')}
            input={<Input name="category" id="category-helper" />}
            fullWidth
          >
            <MenuItem value=''>{LOCALIZATION.SHOP_FILTER_CATEGORY_TYPE_ALL}</MenuItem>
            <MenuItem value='art'>{LOCALIZATION.SHOP_FILTER_CATEGORY_TYPE_ART}</MenuItem>
            <MenuItem value='music'>{LOCALIZATION.SHOP_FILTER_CATEGORY_TYPE_MUSIC}</MenuItem>
            <MenuItem value='design'>{LOCALIZATION.SHOP_FILTER_CATEGORY_TYPE_DESIGN}</MenuItem>
            <MenuItem value='video making'>{LOCALIZATION.SHOP_FILTER_CATEGORY_TYPE_CREATING_A_VIDEO}</MenuItem>
            <MenuItem value='photography'>{LOCALIZATION.SHOP_FILTER_CATEGORY_TYPE_PHOTOGRAPHY}</MenuItem>
          </Select>
        </FormControl>


        <FormControl className='mt-4' fullWidth>
          <InputLabel htmlFor="jobType-helper">{LOCALIZATION.SHOP_ADD_FIND_TITLE}</InputLabel>
          <Input
            name="jobType"
            id="jobType-helper"
            value={this.state.title}
            onChange={this.handleChange('title')}
          />
        </FormControl>

        <div className='d-flex justify-content-center'>
          <Button
            onClick={() => filterItem(this.state)}
            className='mt-4'
            color="primary"
            variant="contained"
          >
            {LOCALIZATION.SHOP_SEARCH_BUTTON}
          </Button>
        </div>
      </div>
    )
  }
}
