import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Paper from '@material-ui/core/Paper';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';

import currencies from '../../../static/Сurrency'
import paymentOptions from '../../../static/paymentOptions';
import Snackbar from '../../Publick/Snackbar';
import Firebase from '../../../../FireBase/FireBase';
import isEmpty from '../../../../auth/is-empty';
import {LOCALIZATION} from "../../../../Localization/localization";

class ResponsiveDialog extends React.Component {
  state = {
    open: false,
    title: '',
    text: '',
    category: '',
    cost: '',
    titleImage: '',
    currency: '',
    paymentOptions: [],
    send: false,
    imageList: []
  };

  uploadImgList = (vul) => {
    const imageList = [...this.state.imageList];
    imageList.push({imgPath: vul})
    this.setState({imageList: imageList})
    //this.setState({titleForImageList: ''})
    //this.setState({imageTitle: vul})
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  componentDidMount(){
    if(this.props.reduct){
      this.setState({
        title: this.props.work.title,
        titleImage: this.props.work.titleImage,
        text: this.props.work.text,
        category: this.props.work.category,
        currency: this.props.work.currency,
        imageList: this.props.work.imageList,
        cost: this.props.work.cost,
        paymentOptions: this.props.work.paymentOptions,
      })
    }
  }

  upload = (vul) => {
    this.setState({titleImage: vul})
  };

  reductWork = () => {
    const updateShopItem = {
      title: this.state.title,
      text: this.state.text,
      category: this.state.category,
      cost: this.state.cost,
      currency: this.state.currency,
      imageList: this.state.imageList,
      paymentOptions: this.state.paymentOptions,
    }
    axios.put(`/api/shop/item/${this.props.dop.match.params.item_id}`, updateShopItem)
      .then(window.location.reload(true))
  }

  addWork = () => {
    const shopItem = {
      title: this.state.title,
      text: this.state.text,
      category: this.state.category,
      imageList: this.state.imageList,
      currency: this.state.currency,
      cost: this.state.cost,
      paymentOptions: this.state.paymentOptions,
      titleImage: this.state.titleImage
    };
    axios.post('/api/shop/', shopItem)
      .then(vul => {
        this.setState({send: true})
        this.timeout = setTimeout(() => window.location.reload(true), 3000)
      });
  };

  componentWillUnmount(){
    clearTimeout(this.timeout)
  };

  errors = (vul) => {
    const errors = {};

    if(vul.title.length <= 3)
      errors.title = LOCALIZATION.SHOP_ADD_VALIDATION_TITLE;

    if(vul.text.length <= 20)
      errors.text = LOCALIZATION.SHOP_ADD_VALIDATION_TASK;

    if(vul.category.length <= 1)
      errors.category = LOCALIZATION.SHOP_ADD_VALIDATION_CATEGORY;

    if(vul.cost.length <= 0)
      errors.cost = LOCALIZATION.SHOP_ADD_VALIDATION_COST;

    if(vul.titleImage.length <= 3)
      errors.titleImage = LOCALIZATION.SHOP_ADD_VALIDATION_MAIN_IMAGE;

    if(vul.currency.length <= 0)
      errors.currency = LOCALIZATION.SHOP_ADD_VALIDATION_CURRENCY;

    if(vul.paymentOptions.length <= 0)
      errors.currency = LOCALIZATION.SHOP_ADD_VALIDATION_PAYMENT_TYPE;

    return errors
  };

  render() {
    const { fullScreen } = this.props;
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
      PaperProps: {
        style: {
          maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
          width: 250,
        },
      },
    };

    const errors = this.errors(this.state);

    return (
      <div>
        {
          this.props.reduct
          ?
            <Button onClick={this.handleClickOpen} className='mb-2' variant="outlined" style={{color: 'green', borderColor: 'green'}}>
              Редактировать
            </Button>
          :
            <Button style={{color: 'white', borderColor: 'white'}} variant="outlined" onClick={this.handleClickOpen}>
              {LOCALIZATION.SHOP_ADD_PRODUCT}
            </Button>
        }
        <Dialog
          fullScreen={fullScreen}
          fullWidth
          maxWidth={'sm'}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{LOCALIZATION.SHOP_ADD_PRODUCT}</DialogTitle>

          <DialogContent>
            <Paper elevation={1} className='p-4' style={{textAlign: 'center'}}>
              <Typography className='mb-1' variant="h5" component="h3">
                {LOCALIZATION.SHOP_ADD_MAIN_IMAGE}
              </Typography>
              <img style={{height: 200}} src={this.state.titleImage}/>
              <Firebase addNews={true} upload={this.upload} />
              {errors.titleImage && (<div className='text-danger'>{errors.titleImage}</div>)}
            </Paper>

            <Paper elevation={1} className='p-4 mt-4' style={{textAlign: 'center'}}>
                <Typography className='mb-1' variant="h5" component="h3">
                  {LOCALIZATION.SHOP_ADD_ALBUM}
                </Typography>
                {this.state.imageList.map((vul, key) => {
                    return(
                        <img className='img-thumbnail' style={{height: 200}} src={vul.imgPath} alt={'thumbnail'}/>
                    )
                })}
                <Firebase uploadImgList={this.uploadImgList} />
            </Paper>

            <TextField
                id="standard-title"
                label={LOCALIZATION.SHOP_ADD_TITLE}
                fullWidth
                value={this.state.title}
                onChange={this.handleChange('title')}
                margin="normal"
            />
            {errors.title && (<div className='text-danger'>{errors.title}</div>)}

            <FormControl className='mt-4' fullWidth>
                <InputLabel htmlFor="category-helper">{LOCALIZATION.SHOP_ADD_CATEGORY}</InputLabel>
                <Select
                    value={this.state.category}
                    onChange={this.handleChange('category')}
                    input={<Input name="category" id="category-helper" />}
                    fullWidth
                >
                    <MenuItem value='art'>{LOCALIZATION.SHOP_ADD_CATEGORY_ART}</MenuItem>
                    <MenuItem value='music'>{LOCALIZATION.SHOP_ADD_CATEGORY_MUSIC}</MenuItem>
                    <MenuItem value='3d model'>{LOCALIZATION.SHOP_ADD_CATEGORY_3D_MODEL}</MenuItem>
                    <MenuItem value='design'>{LOCALIZATION.SHOP_ADD_CATEGORY_DESIGN}</MenuItem>
                    <MenuItem value='video making'>{LOCALIZATION.SHOP_ADD_CATEGORY_CREATING_A_VIDEO}</MenuItem>
                    <MenuItem value='photography'>{LOCALIZATION.SHOP_ADD_CATEGORY_PHOTOGRAPHY}</MenuItem>
                </Select>
            </FormControl>
            {errors.category && (<div className='text-danger'>{errors.category}</div>)}

            <FormControl className='mt-3' fullWidth>
                <InputLabel htmlFor="select-multiple-checkbox">{LOCALIZATION.SHOP_ADD_PAYMENT_OPTIONS}</InputLabel>
                <Select
                  multiple
                  fullWidth
                  value={this.state.paymentOptions}
                  onChange={this.handleChange('paymentOptions')}
                  input={<Input id="select-multiple-checkbox" />}
                  renderValue={selected => selected.join(', ')}
                  MenuProps={MenuProps}
                  >
                  {paymentOptions.map(name => (
                      <MenuItem key={name} value={name}>
                      <Checkbox checked={this.state.paymentOptions.indexOf(name) > -1} />
                      <ListItemText primary={name} />
                      </MenuItem>
                  ))}
                </Select>
            </FormControl>
            {errors.paymentOptions && (<div className='text-danger'>{errors.paymentOptions}</div>)}

            <div className='d-flex'>
                <div>
                  <TextField
                      id="standard-select-currency"
                      select
                      style={{width: 150}}
                      label={LOCALIZATION.SHOP_ADD_CURRENCY}
                      value={this.state.currency}
                      onChange={this.handleChange('currency')}
                      SelectProps={{
                        MenuProps: {},
                      }}
                      margin="normal"
                      >
                      {currencies.map(option => (
                          <MenuItem key={option.value} value={option.value}>
                          {option.value + ` (${option.label})`}
                          </MenuItem>
                      ))}
                  </TextField>
                  {errors.currency && (<div className='text-danger'>{errors.currency}</div>)}
                </div>

                <div style={{width: '100%'}}>
                  <TextField
                      style={{marginLeft: 10}}
                      id="standard-title"
                      label={LOCALIZATION.SHOP_ADD_PRICE}
                      fullWidth
                      value={this.state.cost}
                      onChange={this.handleChange('cost')}
                      margin="normal"
                  />
                  {errors.cost && (<div className='text-danger'>{errors.cost}</div>)}
                </div>
            </div>

            <TextField
              id="standard-multiline-flexible"
              label={LOCALIZATION.SHOP_ADD_DESCRIPTION}
              multiline
              fullWidth
              rows='5'
              rowsMax="10"
              value={this.state.text}
              onChange={this.handleChange('text')}
              margin="normal"
            />
            {errors.text && (<div className='text-danger'>{errors.text}</div>)}

          </DialogContent>
          <DialogActions>
            {
              this.props.reduct
              ?
                <Button onClick={this.reductWork} color="primary">
                  Сохранить
                </Button>
              :
                <>
                {isEmpty(errors)
                  ?
                  <Button onClick={this.addWork} color="primary">
                    Опубликовать
                  </Button>
                  :
                  <Button disabled color="primary">
                    Опубликовать
                  </Button>
                }
                </>
            }
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Закрыть
            </Button>
          </DialogActions>
        </Dialog>
        {
          this.state.send
          ?
           <Snackbar textMessage='Опубликованно'/>
          :
            null
        }
      </div>
    );
  }
}

ResponsiveDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default withMobileDialog()(ResponsiveDialog);
