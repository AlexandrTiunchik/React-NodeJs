import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';
import NavigationIcon from '@material-ui/icons/Navigation';
import MailIcon from '@material-ui/icons/Mail';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
});

/**
 * @return {number}
 */
function ProfileButton(props) {
  const { classes } = props;
  if(props.name === 'add'){
      return(
        <Fab color="primary" onClick={props.onClick} aria-label="Добавить" className={classes.fab}>
            <AddIcon />
       </Fab>
      )
  } else if(props.name === 'edit') {
    return(
      <Fab color="secondary" onClick={props.onClick} aria-label="Изменить" className={classes.fab}>
          <Icon>edit_icon</Icon>
      </Fab>
    )
  } else if(props.name === 'message'){
    return(
        <Fab variant="extended" className={classes.fab}>
            Подписаться
        </Fab>
    )
  } else if(props.name === 'headerMessage'){
    return (
      <IconButton onClick={props.onClick} color="inherit">
        <Badge className={classes.margin} badgeContent={0} color="primary">
          <MailIcon />
        </Badge>
      </IconButton>
  )} else if(props.name === 'headerNotification'){
    return(
      1
    )} else {
    return null
  }
}

ProfileButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProfileButton);
