import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import '../../../../App.css';
import {Link} from "react-router-dom";
import {LOCALIZATION} from "../../../../Localization/localization";

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
});

function AlignItemsList(props) {
  const { classes, subscription } = props;

  return (
    <List className={classes.root}>
      {subscription && subscription.map((sub) => (
        <ListItem style={{width: '100%', maxWidth: "100%"}}>
          <ListItemAvatar>
            <Link to={sub.user._id}>
              <Avatar src={sub.user.avatar}/>
            </Link>
          </ListItemAvatar>
          <ListItemText
            primary={`${LOCALIZATION.PROFILE_TAB_HOME}: ${sub.title}`}
            secondary={
              <React.Fragment>
                <Typography component="span" className={classes.inline} color="textPrimary">
                  {sub.user.name}
                </Typography>
                 :  {new Date(sub.date).toDateString()}
              </React.Fragment>
            }
          />
          <div className="d-flex align-items-center justify-content-center">
            <a target="_blank" href={sub.contentRef}>
              <img style={{height: 50, width: 50}} src={sub.contentRef}/>
            </a>
          </div>
        </ListItem>
      ))}
    </List>
  );
}

AlignItemsList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AlignItemsList);
