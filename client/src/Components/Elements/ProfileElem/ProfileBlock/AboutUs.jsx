import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {LOCALIZATION} from "../../../../Localization/localization";

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});


function AboutUs(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_TITLE}</Typography>
        </ExpansionPanelSummary>
          <div className='d-flex row pl-4 pr-4'>
            <div className='col-md-5 col-sm-12 col-lg-3'>
              <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_NAME}:</strong> {props.info.firstname}</p>
              <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_SURNAME}:</strong> {props.info.surname}</p>
              <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_GENDER}:</strong> {props.info.gender}</p>
              <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_BIRTHDAY}:</strong> {props.info.date}</p>
              {
                props.info.single !== null && props.info.multi !== '' && props.info.multi !== undefined && props.info.single !== undefined
                ?
                  <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_COUNTRY}:</strong> {props.info.single.value}</p>
                :
                <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_COUNTRY}:</strong> </p>
              }
            </div>
            <div className='col-sm-12 col-md-7 col-lg-9' style={{width: '300'}}>
              <p><strong>{LOCALIZATION.PROFILE_TAB_ABOUT_ME_INFORMATION_ABOUT_ME}:</strong> {props.info.aboutMe}</p>
            </div>
          </div>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>{LOCALIZATION.PROFILE_TAB_EXPERIENCE_TITLE}</Typography>
        </ExpansionPanelSummary>
          <div className='pr-4 pl-4 mb-3'>
            <div className='mb-1'>
              <span ><strong>{LOCALIZATION.PROFILE_TAB_EXPERIENCE_SKILLS}: </strong></span>
              {
                <React.Fragment>
                  {
                    props.info.multi !== null && props.info.multi !== '' && props.info.multi !== undefined
                    ?
                      props.info.multi.map((vul, key) => {
                        return <span key={key}>{vul.label}, </span>
                      })
                    :
                      null
                  }
                </React.Fragment>
              }
            </div>
            {/* <p>1 Work: </p>
            <p>2 Work: </p>
            <p>3 Work: </p>
            <p>4 Work: </p> */}
          </div>
      </ExpansionPanel>

      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>{LOCALIZATION.PROFILE_TAB_CONTACTS_TITLE}</Typography>
        </ExpansionPanelSummary>
            <div className='pr-4 pl-4'>
            {
              props.info.social !== null && props.info.social !== '' && props.info.social !== undefined
              ?
                <div>
                  {
                    props.info.social.map((vul, key) => {
                      return <p key={key}>{vul.source}: <a href={vul.link}>{vul.link}</a></p>
                    })
                  }
                </div>
              :
                null
            }
            </div>
      </ExpansionPanel>
    </div>
  );
}

AboutUs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AboutUs);
