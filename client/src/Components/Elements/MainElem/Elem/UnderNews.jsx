import React from 'react';
import withWidth from '@material-ui/core/withWidth';
import UnderElem from './UnderElem';


function WithWidth(props) {
  const { width } = props;

  const displaySize = {
    xs: 3,
    sm: 4,
    md: 3,
    lg: 4,
    xl: 5,
  };

  return <UnderElem key='3a' arrNews={newsSlice(props.news, displaySize[width])} />
}

function newsSlice(newsList, count){
  const news = [];
  news.music = newsList.music.slice(0, count);
  news.art = newsList.art.slice(0, count);
  news.film = newsList.film.slice(0, count);
  return news;
}

export default withWidth()(WithWidth);
