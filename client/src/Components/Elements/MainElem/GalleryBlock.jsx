import React, {useState} from "react";
import useAsyncEffect from "use-async-effect";
import axios from 'axios';
import styles from './style.module.css';

function GalleryBlock() {
  const [imageState, setImageState] = useState(null);

  useAsyncEffect(async () => {
    const response = await axios.get(`/api/galery/image`);
    setImageState(response.data);
  }, []);

  return (
    <div>
      <div
        style={{overflow: 'auto'}}
        className="d-flex justify-content-center"
      >
        {imageState && imageState.map(image => (
          <div key={image._id}>
            <div
              style={{backgroundImage: `url(${image.contentRef})`}}
              className={`img-thumbnail ${styles.img}`}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default GalleryBlock;
