import React from "react";
import TextField from "@material-ui/core/TextField";

export default function WorkRequest(props) {
  return (
    <div>
      <TextField
        onChange={props.onChange}
        id="outlined-dense-multiline"
        label="Запрос работы"
        margin="dense"
        fullWidth
        variant="outlined"
        multiline
        value={props.text}
        rows={10}
        rowsMax={20}
      />
    </div>
  );
}
