import React, { Component } from 'react'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import {LOCALIZATION} from "../../../Localization/localization";

export default class JobSelect extends Component {
  state = {
    category: '',
    jobType: '',
    sortBy: ''
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  componentDidMount() {
    this.setState({
      ...this.state,
      category: this.props.category
    })
  }

  render() {
    return (
      <div className='p-2'>
        <FormControl fullWidth>
          <InputLabel htmlFor="sortBy-helper">{LOCALIZATION.LABOR_EXCHANGE_SORTING_LABEL}</InputLabel>
          <Select
            value={this.state.sortBy}
            onChange={this.handleChange('sortBy')}
            input={<Input name="sortBy" id="sortBy-helper" />}
            fullWidth
          >
            <MenuItem value='date'>{LOCALIZATION.LABOR_EXCHANGE_SORTING_TYPE_DATE}</MenuItem>
            <MenuItem value='salary'>{LOCALIZATION.LABOR_EXCHANGE_SORTING_TYPE_COST}</MenuItem>
          </Select>
        </FormControl>

        <FormControl className='mt-4' fullWidth>
          <InputLabel htmlFor="category-helper">{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_LABEL}</InputLabel>
          <Select
            value={this.state.category}
            onChange={this.handleChange('category')}
            input={<Input name="category" id="category-helper" />}
            fullWidth
          >
            <MenuItem value=''>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_ALL}</MenuItem>
            <MenuItem value='art'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_ART}</MenuItem>
            <MenuItem value='music'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_MUSIC}</MenuItem>
            <MenuItem value='3d model'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_3D_MODEL}</MenuItem>
            <MenuItem value='design'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_DESIGN}</MenuItem>
            <MenuItem value='video making'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_CREATING_A_VIDEO}</MenuItem>
            <MenuItem value='photography'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_CATEGORY_TYPE_PHOTOGRAPHY}</MenuItem>
          </Select>
        </FormControl>


        <FormControl className='mt-4' fullWidth>
          <InputLabel htmlFor="jobType-helper">{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_LABEL}</InputLabel>
          <Select
            value={this.state.jobType}
            onChange={this.handleChange('jobType')}
            input={<Input name="jobType" id="jobType-helper" />}
            fullWidth
          >
            <MenuItem value=''>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_ALL}</MenuItem>
            <MenuItem value='full-time'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_FULL_TIME}</MenuItem>
            <MenuItem value='contract'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_FULL_CONTRACT}</MenuItem>
            <MenuItem value='part-time'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_FULL_PART_TIME}</MenuItem>
            <MenuItem value='commission'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_FULL_PART_COMMISSION}</MenuItem>
            <MenuItem value='temporary'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_FULL_PART_TEMPORARY}</MenuItem>
            <MenuItem value='internship'>{LOCALIZATION.LABOR_EXCHANGE_FILTERING_WORK_TYPE_TYPE_FULL_PART_INTERNSHIP}</MenuItem>
          </Select>
        </FormControl>

        <div className='d-flex justify-content-center'>
            <Button onClick={
                () => this.props.workSearch(this.state.category, this.state.jobType, this.state.sortBy)
              } className='mt-4' color="primary" variant="contained">{LOCALIZATION.LABOR_EXCHANGE_FILTERING_SORTING_SEARCH_BUTTON}</Button>
        </div>
      </div>
    )
  }
}
