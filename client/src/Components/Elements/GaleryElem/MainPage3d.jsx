import React, {useState, useRef} from 'react';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import { Canvas, useThree, useRender, useLoader, extend } from 'react-three-fiber'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'

extend({ OrbitControls })
const Controls = props => {
  const { gl, camera } = useThree()
  const ref = useRef()
  useRender(() => ref.current.update())
  return <orbitControls ref={ref} args={[camera, gl.domElement]} {...props} />
}

function Model({ url }) {
  const model = useLoader(GLTFLoader, url, loader => {
    const dracoLoader = new DRACOLoader()
    dracoLoader.setDecoderPath(`/draco-gltf/`)
    loader.setDRACOLoader(dracoLoader)
  })
  return (
    <group rotation={[-Math.PI / 2, 0, 0]} position={[0, -7, 0]} scale={[7, 7, 7]}>
      {model.map(({ geometry, material }) => {
        // There are two buffergeometries in this gltf
        // Save some GPU by rendering the rocks a little less vivd than the rocket
        const rocks = geometry.index.count < 80000
        const Material = rocks ? 'meshLambertMaterial' : 'meshStandardMaterial'
        return (
          <mesh
            key={geometry.uuid}
            rotation={[Math.PI / 13.5, -Math.PI / 5.8, Math.PI / 5.6]}
            geometry={geometry}
            castShadow={!rocks}
            receiveShadow={!rocks}>
            <Material attach="material" map={material.map} roughness={1} />
          </mesh>
        )
      })}
    </group>
  )
}

function ResponsiveDialog() {
  return (
    <div id="test">
      <div className='mb-2' style={{border: '1px solid #e4e4e4', marginBottom: 10,borderRadius: 10 ,backgroundColor: '#fff'}}>

        <div style={{position: 'relative', height: 700}} className='d-flex justify-content-center'>
          <Canvas style={{backgroundColor: 'black'}} camera={{ position: [0, 0, 15] }} shadowMap>
            <ambientLight intensity={1.5} />
            <pointLight intensity={2} position={[-10, -25, -10]} />
            <spotLight
              castShadow
              intensity={1.25}
              angle={Math.PI / 8}
              position={[25, 25, 15]}
              shadow-mapSize-width={2048}
              shadow-mapSize-height={2048}
            />
            <fog attach="fog" args={['#cc7b32', 16, 20]} />
            <Model url="/scene-draco.gltf" />
            <Controls
              autoRotate
              enablePan={true}
              enableZoom={true}
              enableDamping
              dampingFactor={0.5}
              rotateSpeed={1}
              maxPolarAngle={Math.PI / 2}
              minPolarAngle={Math.PI / 2}
            />
          </Canvas>
        </div>
      </div>
    </div>
  );
}

export default React.memo(withMobileDialog()(ResponsiveDialog));
