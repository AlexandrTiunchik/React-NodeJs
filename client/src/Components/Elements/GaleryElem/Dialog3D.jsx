import React, {Suspense, useState, useRef, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Avatar from '@material-ui/core/Avatar';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import { Canvas, useThree, useRender, useLoader, extend } from 'react-three-fiber'
import { useTransition, a } from 'react-spring'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import VideoList from './VideoList';
import {Link} from 'react-router-dom';
import {LOCALIZATION} from "../../../Localization/localization";

extend({ OrbitControls })
const Controls = props => {
  const { gl, camera } = useThree()
  const ref = useRef()
  useRender(() => ref.current.update());
  return <orbitControls ref={ref} args={[camera, gl.domElement]} {...props} />
};

function Model({ url }) {
  const model = useLoader(GLTFLoader, url, loader => {
    const dracoLoader = new DRACOLoader()
    dracoLoader.setDecoderPath(`/draco-gltf/`);
    loader.setDRACOLoader(dracoLoader)
  });
  return (
    <group rotation={[-Math.PI / 2, 0, 0]} position={[0, -7, 0]} scale={[7, 7, 7]}>
      {model.map(({ geometry, material }) => {
        // There are two buffergeometries in this gltf
        // Save some GPU by rendering the rocks a little less vivd than the rocket
        const rocks = geometry.index.count < 80000
        const Material = rocks ? 'meshLambertMaterial' : 'meshStandardMaterial'
        return (
          <mesh
            key={geometry.uuid}
            rotation={[Math.PI / 13.5, -Math.PI / 5.8, Math.PI / 5.6]}
            geometry={geometry}
            castShadow={!rocks}
            receiveShadow={!rocks}>
            <Material attach="material" map={material.map} roughness={1} />
          </mesh>
        )
      })}
    </group>
  )
}

function Loading() {
  const [finished, set] = useState(false)
  const [width, setWidth] = useState(0)

  const props = useTransition(finished, null, {
    from: { opacity: 1, width: 0 },
    leave: { opacity: 0 },
    update: { width },
  });

  return props.map(
    ({ item: finished, key, props: { opacity, width } }) =>
      !finished && (
        <a.div className="loading" key={key} style={{ opacity }}>
          <div className="loading-bar-container">
            <a.div className="loading-bar" style={{ width }} />
          </div>
        </a.div>
      ),
  )
}

function ResponsiveDialog(props) {
  const [state, setState] = useState({
    load: false,
    open: false,
    video: {
      titleImage: 'https://raw.githubusercontent.com/drcmda/learnwithjason/master/intro.jpg',
      title: 'Космос',
      user: {
        name: 'Alex'
      }
    }
  });

  const handleClickOpen = () => {
    setState({...state, open: true });
  };

  const handleClose = () => {
    setState({...state, open: false, load: false });
  };

  const { fullScreen } = props;

  return (
    <div id="test">
      <VideoList video={state.video} onClick={handleClickOpen}/>
      <Dialog
        fullWidth
        maxWidth='lg'
        fullScreen={fullScreen}
        open={state.open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        {/* <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle> */}
        <DialogContent>

        <div className='mb-2' style={{border: '1px solid #e4e4e4', marginBottom: 10,borderRadius: 10 ,backgroundColor: '#fff'}}>

          <div style={{position: 'relative', height: 700}} className='d-flex justify-content-center'>
            <Canvas style={{backgroundColor: 'black'}} camera={{ position: [0, 0, 15] }} shadowMap>
              <ambientLight intensity={1.5} />
              <pointLight intensity={2} position={[-10, -25, -10]} />
              <spotLight
                castShadow
                intensity={1.25}
                angle={Math.PI / 8}
                position={[25, 25, 15]}
                shadow-mapSize-width={2048}
                shadow-mapSize-height={2048}
              />
              <fog attach="fog" args={['#cc7b32', 16, 20]} />
              <Model url="/scene-draco.gltf" />
              <Controls
                autoRotate
                enablePan={true}
                enableZoom={true}
                enableDamping
                dampingFactor={0.5}
                rotateSpeed={1}
                maxPolarAngle={Math.PI / 2}
                minPolarAngle={Math.PI / 2}
              />
            </Canvas>
          </div>

          <div style={{borderTop: '1px solid #e4e4e4'}} className='pl-3 pr-3 ml-1 mr-1 mt-1 mb-2 d-flex flex-row'>
              <Link className='p-1 align-items-center d-flex flex-row' to={`/profile/user/5c41ceec0df1830bd4f0f393`}>
                <Avatar className='mr-2' alt="Remy Sharp" src='https://firebasestorage.googleapis.com/v0/b/artgalery-fe9fd.appspot.com/o/images%2F02-funny-cat-wallpapercat-wallpaper.jpg?alt=media&token=bf18701b-a07a-4e7b-9213-3742ac3f7491' />
                <span>Alex</span>
              </Link>
          </div>

          <ExpansionPanel style={{boxShadow: 'none'}} className='ml-1 mr-1 mt-1'>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography>{LOCALIZATION.GALLERY_MUSIC_DIALOG}</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                  <Typography>
                  </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
      </div>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            {LOCALIZATION.GALLERY_MUSIC_CLOSE}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default React.memo(withMobileDialog()(ResponsiveDialog));
