import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import { connect } from 'react-redux';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { ListItem } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import ProgressBar from '../Elements/Publick/ProgressBar';
import DeliteDialog from '../Elements/Publick/DeliteDialog';
import AddWork from '../Elements/Work/AddWork';
import WorkRequest from '../Elements/Work/WorkRequest';
import WorkApi from '../../Services/Api/workApi';
import {LOCALIZATION} from "../../Localization/localization";

class WorkItem extends Component {
    state = {
      avatar: '',
      userId: '',
      name: '',
      open: false,
      title: '',
      task: '',
      requirements: '',
      category: '',
      jobType: '',
      currency: '',
      cost: '',
      date: '',
      updateDate: '',
      paymentOptions: [],
      load: false,
      requestText: '',
      checkRequest: false,
      checkedRequest: null,
      workRequestList: null,
      workRequestCount: 0,
    };

    componentWillMount() {
      this.getWork();
    }

    getWork = () => {
      this.setState({ load: false });
      new WorkApi().checkRequest(this.props.match.params.work_id).then((response) => {
        this.setState({
          ...this.state,
          checkRequest: response.data.check,
          checkedRequest: response.data.checkedRequest,
        });
      });

      axios.get(`/api/work/item/${this.props.match.params.work_id}`)
        .then((work) => {
          this.setState({
            updateDate: work.data.updateDate,
            avatar: work.data.user.avatar,
            userId: work.data.user._id,
            name: work.data.user.name,
            title: work.data.title,
            task: work.data.task,
            requirements: work.data.requirements,
            category: work.data.category,
            jobType: work.data.jobType,
            currency: work.data.currency,
            cost: work.data.cost,
            paymentOptions: work.data.paymentOptions,
            date: work.data.date,
            load: true,
            workRequestList: null,
          });
          return work.data;
        }).then((data) => {
          new WorkApi().getWorkRequestList(this.props.match.params.work_id).then(((request) => {
            if (this.props.auth.isAuthenicated && this.props.auth.user.id === this.state.userId) {
              this.setState({
                ...this.state,
                workRequestList: request.data,
              });
            } else {
              this.setState({
                ...this.state,
                workRequestCount: request.data.length,
              });
            }
          }));
        });
    };

    onChange = (e) => {
      this.setState({
        ...this.state,
        requestText: e.target.value,
      });
    };

    createWorkRequest = async () => {
      const data = {
        work: this.props.match.params.work_id,
        user: this.props.auth.user._id,
        text: this.state.requestText,
      };
      await new WorkApi().createWorkRequest(data);
      document.location.reload(true);
    };

    deleteWorkRequest = async () => {
      await new WorkApi().deleteRequest(this.state.checkedRequest[0]._id);
      document.location.reload(true);
    };

    render() {
      return (
        <>
          {
                this.state.load
                  ? (
                    <div
                      className="container rounded shadow"
                      style={{
                        height: '100%', backgroundColor: 'white', marginTop: 60, fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
                      }}
                    >

                      <div className="row d-flex justify-content-between p-2 pt-4 pb-4 align-items-start">
                        <div className="col-md-12 col-lg-7 col-xl-7">
                          <h3>{this.state.title}</h3>
                          <p>
                            {LOCALIZATION.WORK_PROJECT_CATEGORY}
                            {': '}
                            {this.state.category}
                          </p>
                          <h4>{LOCALIZATION.WORK_PROJECT_TASK}</h4>
                          <p className="text-justify" style={{ textIndent: 30 }}>{this.state.task}</p>
                          <h4>{LOCALIZATION.WORK_PROJECT_MANDATORY_REQUIREMENTS}</h4>
                          <p>{this.state.requirements}</p>
                        </div>

                        <div className="col-sm-12 col-md-12 col-lg-4 col-xl-4 shadow rounded p-3 mr-sm-2 ">
                          <div className="p-2 border-bottom"><p>{LOCALIZATION.WORK_PROJECT_INFORMATION_TITLE}</p></div>
                          <div className="d-flex justify-content-between p-2 border-bottom mt-1">
                            <span style={{ color: '#888' }}>
                              {LOCALIZATION.WORK_PROJECT_INFORMATION_COST}
                              {': '}
                            </span>
                            {' '}
                            <span>
                              {this.state.cost}
                              {' '}
                              {this.state.currency}
                            </span>
                          </div>
                          <div className="d-flex justify-content-between p-2 border-bottom mt-1">
                            <span style={{ color: '#888' }}>{LOCALIZATION.WORK_PROJECT_INFORMATION_DEADLINE}: </span>
                            <span>3 month</span>
                          </div>
                          <div className="d-flex justify-content-between p-2 border-bottom mt-1">
                            <div><span style={{ color: '#888' }}>{LOCALIZATION.WORK_PROJECT_INFORMATION_PAYMENT_OPTIONS}:</span></div>
                            <div className="d-flex flex-column text-right">
                              {this.state.paymentOptions.map((vul, key) => <span key={key}>{vul}</span>)}
                            </div>
                          </div>
                          <div className="d-flex justify-content-between p-2 border-bottom mt-1">
                            <span style={{ color: '#888' }}>{LOCALIZATION.WORK_PROJECT_INFORMATION_DATE_OF_PUBLICATION}:</span>
                            {' '}
                            <span>
                              {this.state.date.slice(0, 10)}
                              {' '}
                              {this.state.date.slice(12, 16)}
                            </span>
                          </div>
                          {
                            this.state.updateDate !== '' && this.state.updateDate !== undefined
                              ? (
                                <div className="d-flex justify-content-between p-2 border-bottom mt-1">
                                  <span style={{ color: '#888' }}>{LOCALIZATION.WORK_PROJECT_INFORMATION_UPDATE_DATE}: </span>
                                  {' '}
                                  <span>
                                    {this.state.updateDate.slice(0, 10)}
                                    {' '}
                                    {this.state.updateDate.slice(12, 16)}
                                  </span>
                                </div>
                              )
                              : null
                          }
                          {/* <div className='d-flex justify-content-between p-2 mt-1'><span style={{color: '#888'}}>Был на сайте:</span> <span>2019-02-05 21:12</span></div> */}
                        </div>
                      </div>

                      <div className="p-3 border-top d-flex justify-content-sm-between flex-wrap justify-content-center">
                        <div className="d-flex flex-column ml-1 mr-1">
                          <Link to={`/profile/user/${this.state.userId}`}>
                            <div className="d-flex align-items-center shadow-sm rounded p-2 btn btn-primary justify-content-center">
                              <Avatar className="mr-2" src={this.state.avatar} />
                              <span>{this.state.name}</span>
                            </div>
                          </Link>
                          <p className="mt-3 mb-0 text-center mb-2 mb-sm-0">
                            {LOCALIZATION.WORK_PROJECT_WORK_REQUEST_COUNT}:
                            {' '}
                            {this.state.workRequestCount}
                          </p>
                        </div>

                        {
                                this.props.auth.isAuthenicated
                                  ? (
                                    <div className="ml-1 mr-1 d-flex flex-column ">
                                      {
                                        this.props.auth.user.id === this.state.userId
                                          ? (
                                            <>
                                              <AddWork getWork={this.getWork} dop={this.props} reduct work={this.state} />
                                              <DeliteDialog work={this.props} />
                                            </>
                                          )
                                          : (
                                            <>
                                              {/* <Button className='mb-2' variant="outlined" color="secondary"> */}
                                              {/*    Пожаловаться */}
                                              {/* </Button> */}
                                              {this.state.checkRequest
                                                ? (
                                                  <Button onClick={this.createWorkRequest} variant="outlined" color="primary">
                                                    {LOCALIZATION.WORK_PROJECT_APPLY_WORK_REQUEST}
                                                  </Button>
                                                )
                                                : (
                                                  <Button onClick={this.deleteWorkRequest} variant="outlined" color="primary">
                                                    {LOCALIZATION.WORK_PROJECT_DELETE_WORK_REQUEST}
                                                  </Button>
                                                )}
                                            </>
                                          )
                                    }
                                    </div>
                                  )
                                  : null
                            }
                      </div>

                      {this.props.auth.isAuthenicated && this.props.auth.user.id === this.state.userId
                        ? (
                          <ListItem>
                            {this.state.workRequestList && this.state.workRequestList.map((item) => (
                              <div key={item._id} style={{ flex: 1 }}>
                                <ListItem style={{ flex: 1 }}>
                                  <Link to={`/profile/user/${item.user._id}`}>
                                    <ListItemAvatar>
                                      <Avatar src={item.user.avatar} />
                                    </ListItemAvatar>
                                  </Link>
                                  <ListItemText primary={item.user.name} secondary={item.text} />
                                </ListItem>
                                <Divider variant="fullWidth" />
                              </div>
                            ))}
                          </ListItem>
                        )
                        : (
                          <ListItem>
                            {this.state.checkedRequest && this.state.checkedRequest.map((item) => (
                              <div key={item._id} style={{ flex: 1 }}>
                                <strong><p>{LOCALIZATION.WORK_PROJECT_YOUR_APPLICATION}</p></strong>
                                <ListItem style={{ flex: 1 }}>
                                  <Link to={`/profile/user/${item.user._id}`}>
                                    <ListItemAvatar>
                                      <Avatar src={item.user.avatar} />
                                    </ListItemAvatar>
                                  </Link>
                                  <ListItemText primary={item.user.name} secondary={item.text} />
                                </ListItem>
                                <Divider variant="fullWidth" />
                              </div>
                            ))}
                          </ListItem>
                        )}


                      {this.state.checkRequest && this.props.auth.isAuthenicated && this.props.auth.user.id !== this.state.userId
                        ? (
                          <div className="pb-4">
                            <WorkRequest
                              text={this.state.requestText}
                              onChange={this.onChange}
                            />
                          </div>
                        )
                        : null}
                    </div>
                  )
                  : (
                    <div style={{ height: '95vh' }} className="d-flex justify-content-center">
                      <ProgressBar />
                    </div>
                  )
            }
        </>
      );
    }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(WorkItem);
