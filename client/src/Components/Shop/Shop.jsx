import React, { Component } from 'react'
import CardProduct from '../Elements/Shop/CardProduct';
import axios from 'axios';
import { connect } from 'react-redux';

import ShopFilter from '../Elements/Shop/Elem/ShopFilter';
import AddShopItem from '../Elements/Shop/Elem/AddShopItem';
import ProgressBar from '../Elements/Publick/ProgressBar';
import {LOCALIZATION} from "../../Localization/localization";

class Shop extends Component {
  state = {
    price: 0,
    shopItems: '',
    isLoad: false,
  };

  onChange = (e) =>{
    this.setState({
      price: e.target.value
    })
  };

  componentWillMount(){
    axios.get('/api/shop/')
      .then(vul => this.setState({
        shopItems: vul.data.filter(item => item.category !== 'design'),
        isLoad: true
      }))
  };

  shopFilter = (param) => {
    axios.post('/api/shop/search', param)
      .then(ShopItem => {
        this.setState({
          shopItems: ShopItem.data.ShopItem,
          ...ShopItem.data.find,
          load: true
        })
      })
  };

  shopItem(item) {
    return (
      <div key={item.id} className='d-flex col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 justify-content-center mb-2'>
        <CardProduct shopItems={item}/>
      </div>
    )
  };

  render() {
    return (
      <>
      {
          this.state.isLoad
          ?
            <div style={{height: '100%', marginTop: 70}} >
              <div className='mt-5 container' >
                <div style={{background: '#525252'}} className='d-flex justify-content-between mb-4 align-items-center p-2 rounded'>
                  <h3 style={{color: 'white'}}>{LOCALIZATION.SHOP_TITLE}</h3>
                  {
                    this.props.auth.isAuthenicated
                      ?
                        <AddShopItem />
                      :
                      null
                  }
                </div>

                <div className='row d-flex justify-content-center'>
                  <div className='col-12 col-lg-3'>
                    <ShopFilter filterItem={this.shopFilter} />
                  </div>
                  <div className='col-12 flex-wrap col-lg-9 justify-content-evenly row'>
                    {this.state.shopItems.map(this.shopItem)}
                  </div>
                </div>
              </div>
            </div>
          :
            <div style={{height: '95vh'}} className='d-flex justify-content-center'>
              <ProgressBar />
            </div>
      }
    </>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(Shop);
