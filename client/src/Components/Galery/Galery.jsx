import React, { Component } from 'react'
import Tubs from '../Elements/GaleryElem/Tubs';

export default class Galery extends Component {
  render() {
    return (
      <div style={{marginTop: 60}}>
        <Tubs />
      </div>
    )
  }
}
