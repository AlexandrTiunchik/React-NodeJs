import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import BasketIcon from '@material-ui/icons/ShoppingBasket';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import MoreIcon from '@material-ui/icons/MoreVert';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

//Drawer
import Drawer from '../../Elements/HeaderDrawer/HeaderDrawer';
import Message from '../../Elements/ProfileElem/Dialog/Message';
import RegisterLogin from '../../Elements/RegisterLogin/RegisterLogin';

//Checker
import {logoutUser} from '../../../redux/actions/authActions';
import Button from "../../Elements/MainElem/Elem/CustomButtons/Button";
import withRouter from "react-router/withRouter";
import {LOCALIZATION} from "../../../Localization/localization";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    display: 'flex',
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    '&:hover': {
      cursor: 'pointer'
    },
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
   // paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});

class PrimarySearchAppBar extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    searchValue: '',
    language: '',
  };

  componentDidMount() {
    this.setState({
      ...this.state,
      language: LOCALIZATION.getLanguage(),
    })
  }

  changeLanguage = (event)  => {
    const { value } = event.target;
    localStorage.setItem('lang', value);
    document.location.reload(true);
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  MenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
    this.props.logoutUser();
  };

  handleInput = (e) => {
    this.setState({
      ...this.state,
      searchValue: e.target.value
    })
  };

  render() {
    const { anchorEl, mobileMoreAnchorEl } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <Link className='styleforLink' to={`/profile/user/${this.props.auth.user.id}`}>
          <MenuItem onClick={this.handleMenuClose}>{LOCALIZATION.HEADER_USER_CATEGORY_PROFILE}</MenuItem>
        </Link>

        <Link className='styleforLink' to={`/manager`}>
          <MenuItem onClick={this.handleMenuClose}>{LOCALIZATION.HEADER_USER_CATEGORY_MANAGER}</MenuItem>
        </Link>

        {this.props.auth.user.admin
          ?
            <Link className='styleforLink' to={`/admin`}>
            <MenuItem onClick={this.handleMenuClose}>{LOCALIZATION.HEADER_USER_CATEGORY_ADMINISTRATOR}</MenuItem>
            </Link>
          :
            null
        }

        <MenuItem onClick={this.MenuClose}>{LOCALIZATION.HEADER_USER_CATEGORY_LOGOUT}</MenuItem>
      </Menu>
    );

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        <FormControl style={{width: '100%'}} className='justify-content-center pl-4 pr-4'>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={this.state.language}
            onChange={this.changeLanguage}
          >
            <MenuItem value='EN'>EN</MenuItem>
            <MenuItem value='RU'>RU</MenuItem>
          </Select>
        </FormControl>

        <MenuItem onClick={() => this.props.history.push('/basket')}>
          <IconButton color="inherit">
            <BasketIcon />
          </IconButton>
          <p>Корзина</p>
        </MenuItem>
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton color="inherit">
            <AccountCircle />
          </IconButton>
          <p>Профиль</p>
        </MenuItem>
      </Menu>
    );

    return (
      <div className={classes.root}>
        <AppBar style={{backgroundColor: '#181818'}} position="fixed">
          <Toolbar>
            <Drawer />
            <Link style={{color: 'white'}} to='/'>
              <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                Art Gallery
              </Typography>
            </Link>
            <div className={classes.search}>
              <Button onClick={() => this.props.history.push(
                  {
                    pathname: '/search',
                    search: `?value=${this.state.searchValue}`
                  }
                  )}>
                  <SearchIcon cursor={2}/>
              </Button>
              <InputBase
                placeholder={LOCALIZATION.MAIN_PAGE_SEARCH}
                onChange={this.handleInput}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
              />
            </div>
            <div className={classes.grow} />
            {
                this.props.auth.isAuthenicated
                ?
                  <>
                    <div className={classes.sectionDesktop}>

                      <FormControl className='justify-content-center'>
                        <Select
                          style={{color: 'white'}}
                          labelId="demo-simple-select-outlined-label"
                          id="demo-simple-select-outlined"
                          value={this.state.language}
                          onChange={this.changeLanguage}

                        >
                          <MenuItem value='EN'>EN</MenuItem>
                          <MenuItem value='RU'>RU</MenuItem>
                        </Select>
                      </FormControl>

                      <IconButton onClick={() => this.props.history.push('/basket')} color="inherit">
                        <BasketIcon />
                      </IconButton>

                      <IconButton
                        aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                        aria-haspopup="true"
                        onClick={this.handleProfileMenuOpen}
                        color="inherit"
                      >
                        <AccountCircle />
                      </IconButton>
                    </div>
                    <div className={classes.sectionMobile}>
                      <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                        <MoreIcon />
                      </IconButton>
                    </div>
                  </>
                : (
                  <>
                    <FormControl className='justify-content-center'>
                      <Select
                        style={{color: 'white'}}
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={this.state.language}
                        onChange={this.changeLanguage}

                      >
                        <MenuItem value='EN'>EN</MenuItem>
                        <MenuItem value='RU'>RU</MenuItem>
                      </Select>
                    </FormControl>

                    <RegisterLogin />
                  </>
                  )
            }
          </Toolbar>
        </AppBar>

        {
            this.props.auth.isAuthenicated
            ?
              <React.Fragment>
                  {renderMenu},
                  {renderMobileMenu}
              </React.Fragment>
            :
              null
        }

      </div>
    );
  }
}



const mapStateToProps = (state) => ({
  auth: state.auth,
})

export default withRouter(connect(mapStateToProps, {logoutUser})(withStyles(styles)(PrimarySearchAppBar)));
