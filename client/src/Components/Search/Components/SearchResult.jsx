import React, {memo} from "react";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';


function SearchResult(props) {
  const {
    itemImgPath,
    itemTitlePath,
    itemDescriptionPath
  } = props;

  return (
    <div>
      {props.searchResult.map((item, key) => (
        <Link key={key} to={`${props.url}${item._id}`}>
          <div className="rounded border p-3 d-flex row m-1">
            {itemImgPath && (
              <div className="d-flex align-items-center col-12 col-md-2 justify-content-center">
                {item[itemImgPath] && <img className="img-fluid" src={item[itemImgPath]} />}
              </div>
            )}
            <div className="d-flex flex-column col-12 col-md-10">
              {item[itemTitlePath] && <h2>{item[itemTitlePath]}</h2>}
              {item[itemDescriptionPath] && <p>{item[itemDescriptionPath].slice(0, 220)}</p>}
            </div>
          </div>
        </Link>
      ))}
    </div>
  )
}

SearchResult.propTypes = {
  searchResult: PropTypes.array,
  itemTitlePath: PropTypes.string,
  itemDescriptionPath: PropTypes.string,
  itemImgPath: PropTypes.string,
  url: PropTypes.string,
};

export default memo(SearchResult);
