import React from "react";
import {LOCALIZATION} from "../../../Localization/localization";

export const SEARCH_CATEGORY = [
  {
    category: 'users',
    title: LOCALIZATION.SEARCH_CATEGORY_USERS,
    itemImgPath: 'avatar',
    itemTitlePath: 'name',
    url: '/profile/user/',
  },
  {
    category: 'news',
    title: LOCALIZATION.SEARCH_CATEGORY_NEWS,
    itemImgPath: 'image',
    itemDescriptionPath: 'text',
    itemTitlePath: 'name',
    url: '/profile/user/',
  },
  {
    category: 'shopItem',
    title: LOCALIZATION.SEARCH_CATEGORY_TRADING_FLOOR,
    itemImgPath: 'titleImage',
    itemDescriptionPath: 'text',
    itemTitlePath: 'title',
    url: '/shop/item/',
  },
  {
    category: 'work',
    title: LOCALIZATION.SEARCH_CATEGORY_LABOR_EXCHANGE,
    itemDescriptionPath: 'task',
    itemTitlePath: 'title',
    url: '/work/',
  },
];