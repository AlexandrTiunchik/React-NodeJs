import React, {memo, useEffect, useState} from "react";
import axios from 'axios';
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";

import SearchResult from "./Components/SearchResult";
import {SEARCH_CATEGORY} from './Constants/searchCategory';
import useAsyncEffect from "use-async-effect";
import {LOCALIZATION} from "../../Localization/localization";


function Search(props) {
  const [categoryState, setCategory] = useState('all');
  const [totalSearchState, setTotalSearch] = useState(0);
  const [searchState, setSearch] = useState({
    users: [],
    news: [],
    shopItem: [],
    work: [],
    userWork: [],
  });

  useAsyncEffect(async () => {
    const responseResult = await axios.get('/api/search', {params: {value: props.location.search.split('=')[1]}});
    setSearch({
      ...searchState,
      ...responseResult.data,
    });
  }, []);

  useAsyncEffect(() => {
    findTotalSearchResult();
  }, [searchState]);

  const selectCategory = (event) => {
    setCategory(event.target.dataset.category);
  };

  const SearchCategoryResult = (props) => {
    if(categoryState === 'all') {
      return props.children;
    } else if(categoryState === props.category && searchState[props.category].length > 0) {
      return props.children;
    } else {
      return null;
    }
  };

  const findTotalSearchResult = () => {
    let total = 0;

    for (const category in searchState) {
      total += searchState[category].length;
    }
    setTotalSearch(total);
  };

  return (
    <div className="mt-5 container mb-5">

      <div className="row">
        <List component="nav" className="col-12 col-md-3 mr-2">
          {SEARCH_CATEGORY.map(categoryList => (
            <ListItem
              key={categoryList.category}
              onClick={selectCategory}
              button
              divider
              data-category={categoryList.category}
              selected={categoryState === categoryList.category}
            >
              {categoryList.title}: {searchState[categoryList.category]?.length}
            </ListItem>
          ))}
          <ListItem
            key="all"
            onClick={selectCategory}
            button
            divider
            data-category="all"
            selected={categoryState === "all"}
          >
            {LOCALIZATION.SEARCH_CATEGORY_ALL}: {totalSearchState}
          </ListItem>
        </List>

        <div className="col-12 col-md-8">
          {SEARCH_CATEGORY.map(categoryList => (
            <SearchCategoryResult
              key={categoryList.category}
              category={categoryList.category}
            >
              <SearchResult
                searchResult={searchState[categoryList.category]}
                itemImgPath={categoryList.itemImgPath}
                itemDescriptionPath={categoryList.itemDescriptionPath}
                itemTitlePath={categoryList.itemTitlePath}
                url={categoryList.url}
              />
            </SearchCategoryResult>
          ))}
        </div>
      </div>
    </div>
  );
}

export default memo(Search);
