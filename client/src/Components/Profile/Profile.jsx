import React, { Component } from 'react'
import axios from 'axios';
import {connect} from 'react-redux';

import ProfileBlock from '../Elements/ProfileElem/ProfileBlock';
import Avatar from '../Elements/ProfileElem/Avatar';
import AddItem from '../Elements/ProfileElem/Dialog/AddItem';
import EditAbout from '../Elements/ProfileElem/Dialog/EditAbout';
import Message from '../Elements/ProfileElem/Dialog/Message';
import ProgressBar from '../Elements/Publick/ProgressBar';
import Fab from "@material-ui/core/Fab";



class Profile extends Component {
  state = {
    img: '',
    name: '',
    ready: false,
    social: [],
    surname: '',
    firstname: '',
    date: '',
    gender: '',
    aboutMe: '',
    single: '',
    multi: null,
    subscription: false
  };

  Change = name => value => {
    this.setState({
      [name]: value,
    });
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  GetProfileInfo = () => {
    const { url } = this.props.match

    axios.get(`/api${url}`)
      .then(vul => {
        this.setState({
          single: vul.data.location,
          firstname: vul.data.firstname,
          surname: vul.data.surname,
          gender: vul.data.gender,
          avatar: vul.data.user.avatar,
          name: vul.data.user.name,
          aboutMe: vul.data.bio,
          multi: vul.data.skills,
          social: vul.data.social,
          ready: true,
      })
    }
    )
  };

  getSubscriptionMessage = () => {
    return !!this.state.subscription ? 'Отписаться' : 'Подписаться'
  };
  getUserSubscription = () => {
    const { id } = this.props.auth.user;
    axios.get(`/api/subscription`, { id } )
      .then(vul => {
        this.setState({
          ...this.state,
          subscription: vul.data,
        })
      });
  };

  componentWillMount(){
    this.GetProfileInfo();
    this.getUserSubscription();
  }

  subscribe = () => {
    const { user_id } = this.props.match.params;
    const { id } = this.props.auth.user;

    axios.post(`/api/subscription`, { subscribeUser: user_id, id })
    .then(vul => {
    })
  };

  render() {
    return (
      <React.Fragment>
        {
          this.state.ready
          ?
          <div className='container mb-2 mt-5 pt-4'>
            <Avatar img={this.state.avatar}/>
            <h1 className='text-center color-white mb-2'>{this.state.name}</h1>
            <div className='d-flex align-items-center'>
              {
                this.props.auth.user.id === this.props.match.params.user_id
                ?
                  <>
                    <AddItem />
                    <EditAbout
                      GetProfileInfo={this.GetProfileInfo}
                      handleChange={this.handleChange}
                      close={this.GetProfileInfo}
                      Change={this.Change}
                      info={this.state} />
                   </>
              :
              null
            }
            {this.props.auth.isAuthenicated && this.props.auth.user.id !== this.props.match.params.user_id
              ? <Fab
                onClick={this.subscribe}
                variant="extended"
                style={{marginBottom: '20px'}}
              >
                Подписаться
              </Fab>
              : null}
            </div>
            <ProfileBlock info={this.state} currentId={this.props.match.params.user_id}/>
          </div>
          :
          <div style={{height: '95vh'}} className='d-flex justify-content-center'>
            <ProgressBar />
          </div>
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});


export default connect(mapStateToProps)(Profile);
