import OrderApi from "../../Services/Api/orderApi";

export const getSellerOrders = () => async dispatch => {
  try {
    const responseData = await new OrderApi().getSellerOrders();
    return responseData.data;
  } catch (err) {
    throw err;
  }
};

export const getCustomerOrders = () => async dispatch => {
  try {
    const responseData = await new OrderApi().getCustomerOrders();
    return responseData.data;
  } catch (err) {
    throw err;
  }
};

export const createOrder = (data) => async dispatch => {
  try {
    await new OrderApi().addOrder(data);
  } catch (err) {
    throw err;
  }
};


