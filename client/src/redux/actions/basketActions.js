import { SET_BASKET } from '../Types/basketTypes';
import BasketApi from '../../Services/Api/basketApi';

export const setBasket = (basket) => ({
    type: SET_BASKET,
    payload: basket,
});

export const getBasket = () => async dispatch => {
    try {
        const responseData = await new BasketApi().getBasket();
        dispatch(setBasket(responseData.data));
    } catch (err) {
        throw err;
    }
};

export const updateBasket = (basket) => async dispatch => {
    try {
        await new BasketApi().updateBasket(basket);
        dispatch(getBasket());
    } catch (err) {
        throw err;
    }
};

export const deleteItem = (dishes) => async dispatch => {
    try {
        await new BasketApi().deleteItem({ dishes });
        dispatch(getBasket());
    } catch (err) {
        throw err;
    }
};