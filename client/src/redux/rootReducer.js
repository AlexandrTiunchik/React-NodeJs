import { combineReducers } from 'redux';
import authReducer from './reducers/authReducer';
import profileReducer from './reducers/authReducer';
import errorReducer from './reducers/errorReducer';
import basketReducer from './reducers/basketReducer';

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  profile: profileReducer,
  basketReducer,
});