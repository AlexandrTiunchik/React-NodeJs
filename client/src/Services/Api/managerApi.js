import Api from './api';

export default class ManagerApi extends Api {
  MANAGER_URL = '/api/manager';

  async getShopItems() {
    return this.get(`${this.MANAGER_URL}/shop`);
  }

  async getWorkItems() {
    return this.get(`${this.MANAGER_URL}/work`);
  }
}
