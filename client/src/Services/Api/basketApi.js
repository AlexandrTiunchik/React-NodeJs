import Api from './api';

export default class BasketApi extends Api {
  DISH_URL = '/api/basket/';

  async updateBasket(data) {
    return this.put(this.DISH_URL, data);
  }

  async deleteItem(data) {
    return this.put(`${this.DISH_URL}delete`, data);
  }

  async getBasket() {
    return this.get(this.DISH_URL);
  }
}