import Api from './api';

export default class WorkApi extends Api {
  WORK_URL = '/api/work';

  async createWorkRequest(data) {
    return this.post(`${this.WORK_URL}/request`, data);
  }

  async getWorkRequestList(id) {
    return this.get(`${this.WORK_URL}/request/${id}`);
  }

  async checkRequest(id) {
    return this.get(`${this.WORK_URL}/request/check/${id}`);
  }

  async deleteRequest(id) {
    return this.delete(`${this.WORK_URL}/request/${id}`);
  }
}
