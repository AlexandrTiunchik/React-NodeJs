import Api from './api';

export default class OrderApi extends Api {
  ORDER_URL = '/api/order';

  async addOrder(data) {
    return this.post(this.ORDER_URL, data);
  }

  async getSellerOrders() {
    return this.get(`${this.ORDER_URL}/seller`);
  }

  async getCustomerOrders() {
    return this.get(`${this.ORDER_URL}/customer`);
  }
}
