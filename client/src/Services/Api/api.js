import axios from 'axios';

export default class Api {
  async post(url, data) {
    return axios.post(url, data);
  }

  async get(url, params) {
    return axios.get(url, params);
  }

  async put(url, data) {
    return axios.put(url, data);
  }

  async delete(url) {
    return axios.delete(url);
  }
}
