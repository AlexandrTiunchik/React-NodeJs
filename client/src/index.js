import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import JWTDecode from 'jwt-decode';

import App from './App';
import './App.css';
import * as serviceWorker from './serviceWorker';
import store from './redux/store';
import { setAuthToken, setCurrentUser } from './redux/actions/authActions';
import { LOCALIZATION } from './Localization/localization';

if (localStorage.jwtToken){
  setAuthToken(localStorage.jwtToken);
  const decoded = JWTDecode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));
}

const userLang = navigator.language || navigator.userLanguage;

const lang = localStorage.getItem('lang');

if (!lang) {
  if (userLang !== 'ru-RU') {
    LOCALIZATION.setLanguage('EN');
    localStorage.setItem('lang', 'EN');
  } else {
    LOCALIZATION.setLanguage('RU');
    localStorage.setItem('lang', 'EN');
  }
} else {
  LOCALIZATION.setLanguage(lang);
}

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
