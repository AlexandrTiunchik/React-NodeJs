import {LOCALIZATION} from "../Localization/localization";

const quotes = [
  `${LOCALIZATION.MAIN_PAGE_QUOTES_1}`,
  `${LOCALIZATION.MAIN_PAGE_QUOTES_2}`,
  `${LOCALIZATION.MAIN_PAGE_QUOTES_3}`,
  `${LOCALIZATION.MAIN_PAGE_QUOTES_4}`,
];

export default quotes;
